(ns aspex.core
    (:require [reagent.core :as reagent :refer [atom]]
              [goog.events :as events]
              [goog.history.EventType :as EventType]
              [cljsjs.react :as react])
    (:import goog.History))

;; -------------------------
;; Component

(defn raw [x]
  [:div (str @x)])

(defn v-remove [v x]
  (.log js/console (str v x))
  (apply vector (filter #(not= x %) v)))

(defn aspect-list-view [{:keys [title aspects] :or {title "Aspects", aspects []}}]
  (let [appstate (atom [])]
    (defn aspect-view [aspect on-kill]
      [:li
       [:input {:type "button"
                :value "X"
                :class "delete-button"
                :on-click (fn [e] (on-kill))}]
       aspect])
    (defn inbox [{:keys [on-save]}]
      (let [val (atom "")
            save #(let [v (-> @val str clojure.string/trim)]
                    (if-not (empty? v) (on-save v))
                    (reset! val ""))]
        (fn [props]
          [:input
           (merge props
                  {:type "text"
                   :value @val
                   :on-change #(reset! val (-> % .-target .-value))
                   :on-key-down #(case (.-which %)
                                   13 (save)
                                   nil)})])))
    (fn []
      [:div.aspect-list-view
       [:h2 title]
       [inbox {:on-save (fn [a] (swap! appstate conj a))}]
       [:ul
        (for [aspect aspects]
          [:li aspect])
        (for [aspect  @appstate]
          [aspect-view aspect (fn [] (swap! appstate v-remove aspect))])]])))


;; -------------------------
;; Synthesis
(defonce defaults
  [{:title "Setting/Campagin"
    :aspects '["The Future's Amazing but the Past Was Unbelievable"
               "It's the Wild West up Here!"
               "Concrete and Virtual Fought a Horrible War"]}
   {:title "Oliver Torque"
    :aspects ["Anti-Capitalist Geologist"
              "Running with Barrick Interstellar Secrets"
              "Nano-Multi Tool Cyber Arm"
              "Criminal/Radical Network"
              "I know Lazarus's Type"]}
   {:title "Lazarus"
    :aspects ["Fallen from grace political advisor and propaganda expert"
              "Leopold Barrick wants him Dead"
              "Friends in High Places"
              "Pocket VI Personal Assistante (Samantha)"
              "Raised in High Society"]}
   {:title "Situation"
    :aspects []}])


(defn aspex []
  [:div
   [:h1 "Aspex"]
   (for [x defaults]
     [aspect-list-view x])])

(defn init! []
  (reagent/render [aspex] (.getElementById js/document "app")))
